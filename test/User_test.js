let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:5000';

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjo2LCJlbWFpbCI6Impvc2FoYW5keWF2QGdtYWlsLmNvbSIsInJvbGUiOiJzdXBlciJ9LCJpYXQiOjE2Mjk1MTkxOTMsImV4cCI6MTYyOTY5MTk5M30.7Qp30Aalt80I-bznVHAU0_liWqhYmKpff07f2O-tZYA';

describe('Test de usuarios', () => {
    describe('Leer usuario', () => {
        let idUser = 0;
        it('Debe devolver todos los usuarios registrados', (done) => {
            chai.request(url)
            .get('/users')
            .set({ 'Authorization': `jwt ${token}`})
            .end((err, res)  => {
                if(res.body.length > 0){
                  idUser = res.body[0].id;
                }
                expect(res).to.have.status(200);
                done();
            })
        });
        it('Debe devolver los datos de un usuario', (done) => {
            chai.request(url)
            .get(`/users/${idUser}`)
            .set({ 'Authorization': `jwt ${token}`})
            .end((err, res)  => {
                console.log(res.body);
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('name');
                done();
            })
        });
        it('No debe devolver los datos de una cita si el token es inválido', (done) => {
            chai.request(url)
            .get(`/users/6`)
            .set({ 'Authorization': `jwt ${token}y`})
            .end((err, res)  => {
                expect(res).to.have.status(401);
                done();
            })
        });
    });

    describe('Actualizar usuario', () => {
        it('Debe rechazar actualizar si el usuario no existe', (done) => {
            chai.request(url)
            .put('/users/5')
            .set({ 'Authorization': `jwt ${token}`})
            .send({
                name: 'Josahandy',
                lastname: 'Avendaño',
                email: 'josahandyav@gmail.com',
                password: '123abc'
            })
            .end((err, res)  => {
                expect(res).to.have.status(503);
                done();
            })
        });
        it('Debe rechazar actualizar usuario cuando el token sea inválido', (done) => {
            chai.request(url)
            .put('/users/7')
            .set({ 'Authorization': `jwt ${token}abc`})
            .send({
                name: 'Guadalupe3',
                lastname: 'Ocampo3',
                email: 'guadalupeoc3@gmail.com',
                role: 'usuario',
                password: '234533',
            })
            .end((err, res)  => {
                expect(res).to.have.status(401);
                done();
            })
        });
        it('Debe rechazar actualizar usuario si los datos son incorrectos', (done) => {
            chai.request(url)
            .put('/users/8')
            .set({ 'Authorization': `jwt ${token}`})
            .send({
               telephone: 2221246,
               commentary: "Espero su respuesta.",
            })
            .end((err, res)  => {
                expect(res).to.have.status(503);
                done();
            })
        });
    });
    
    describe('Eliminar usuario', () => {
        it('Debe eliminar un usuario', (done) => {
            chai.request(url)
            .delete('/users/70')
            .set({ 'Authorization': `jwt ${token}`})
            .end((err, res)  => {
                expect(res).to.have.status(200);
                done();
            })
        });
        it('Debe rechazar eliminar usuario cuando el token sea inválido', (done) => {
            chai.request(url)
            .delete('/users/6')
            .set({ 'Authorization': `jwt ${token}y`})
            .end((err, res)  => {
                expect(res).to.have.status(401);
                done();
            })
        });
        it('Debe rechazar eliminar usuario, si el usuario no existe', (done) => {
            chai.request(url)
            .delete('/users/1')
            .set({ 'Authorization': `jwt ${token}`})
            .end((err, res)  => {
                expect(res).to.have.status(404);
                done();
            })
        });
    });
    describe('Notificaciones  web-push', () => {
       /* it('Al crear un perfil de usuario debe recuperar las subscription', (done) => {
            chai.request(url)
            .post('/users')
            .send({
                name: 'Guadalupe14',
                lastname: 'Avendaño14',
                email: 'guadalupe14@gmail.com',
                role: 'usuario',
                password: '234533',
            })
            .end((err, res)  => {
                console.log(res.body)
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('usuario');
                expect(res.body).to.have.property('subscription');
                done();
            })
        });*/
        /*it('Al crear un servicio debe recuperar las subscription', (done) => {
            chai.request(url)
            .post('/services')
            .set({ 'Authorization': `jwt ${token}` })
            .send({
                name:'Reparacion de motores',
                cost: 8000,
                time: '5hrs'
            })
            .end((err, res) => {
                console.log(res.body)
              expect(res).to.have.status(200);
              expect(res.body).to.have.property('mensaje');
              expect(res.body).to.have.property('service');
              expect(res.body).to.have.property('subscription');
              done();
            });
        });*/
        it('Al crear una cita de debe recuperar las subscription', (done) => {
            chai.request(url)
            .post('/services/1/appointments')
            .set({ 'Authorization': `jwt ${token}`})
            .send({
                modelCar: "Cavalier 2001" ,
                telephone: 2221246,
                commentary: "Espero su respuesta.",
            })
            .end((err, res)  => {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('mensaje');
                expect(res.body).to.have.property('appointment');
                expect(res.body).to.have.property('subscription');
                done();
            })
        });
        
    });

});
