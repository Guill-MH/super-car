let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:5000';

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjp7ImlkIjo2LCJlbWFpbCI6Impvc2FoYW5keWF2QGdtYWlsLmNvbSIsInJvbGUiOiJzdXBlciJ9LCJpYXQiOjE2Mjk1MTkxOTMsImV4cCI6MTYyOTY5MTk5M30.7Qp30Aalt80I-bznVHAU0_liWqhYmKpff07f2O-tZYA';

describe("Test de services", () => {
    describe("Agregar servicio", () => {
    it('Al crear un servicio debe recuperar las subscription', (done) => {
        chai.request(url)
        .post('/services')
        .set({ 'Authorization': `jwt ${token}` })
        .send({
            name:'Cambio de balatas',
            cost: 3000,
            time: '8hrs'
        })
       .end((err, res) => {
        //console.log(res.body)
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('service');
        expect(res.body).to.have.property('subscription');
        done();
        });
    });
    it('Debe rechazar un servicio cuando no lleva nombre', (done) => {
        chai.request(url)
        .post('/services')
        .set({ 'Authorization': `jwt ${token}` })
        .send({
            cost: 3000,
            time: '8hrs'
        })
        .end((err, res) => {
          //console.log(res)
          //expect(res).to.have.status();
          expect(res.body).to.have.property('errores');
          expect(res.body).to.have.property('message');
          done();
        })
    });
     it('Debe rechazar crear un servicio  cuando: token invalido ', (done) => {
        chai.request(url)
        .post('/services')
        .set({ 'Authorization': `jwt ${token}p` })
        .send({
            name:"Cambio de balatas",
            cost: 3000,
            time: '8hrs',
        })
        .end((err, res) => {
          //console.log(res)
          expect(res).to.have.status(401);
          done();
        })
    });
    });
    describe("Leer servicios", () => {
        let idService = 0;
     it('Debe devolver todos los servicios del sistema', (done) => {
        chai.request(url)
        .get('/services')
        .set({ 'Authorization': `jwt ${token}` })
        .end((err, res) => {
            if(res.body.length > 0) {
                idService = res.body[0].id;
            }
            //console.log(res.body);
            expect(res).to.have.status(200);
            done();
        })
     });
     it('Devolver los datos de un servicio', (done) => {
        chai.request(url)
        .get(`/services/${idService}`)
        .set({ 'Authorization': `jwt ${token}` })
        .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.have.property('name');
            done();
        })
     });
     it('Devolver error si el servicio no existe', (done) => {
        chai.request(url)
        .get(`/services/${idService * 100}`)
        .set({ 'Authorization': `jwt ${token}` })
        .end((err, res) => {
           // console.log(res.body)
            expect(res).to.have.status(404);
            expect(res.body).to.have.property('error');
            done();
        })
     })
    });
    describe("Actualizar un servicio", () => {
        it("Debe actualizar un servicio", (done) => {
            chai.request(url)
        .put('/services/1')
        .set({ 'Authorization': `jwt ${token}` })
        .send({
            name:"Reparacion de Motor 2",
            cost: 1500,
            time: "7hrs",
        })
        .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.have.property('services');
            done();
        })
        });
        it("Retornar error si no se encontro el servicio", (done) => {
            chai.request(url)
            .put('/services/100')
            .set({ 'Authorization': `jwt ${token}` })
            .end((err, res) => {
                //console.log(res.body)
                expect(res).to.have.status(404);
                expect(res.body).to.have.property('error');
                done();
            })
        })
        it("Debe retornar error si el costo lleva string", (done) => {
            chai.request(url)
            .put('/services/3')
            .set({ 'Authorization': `jwt ${token}` })
            .send({
                name:"Reparacion de Motor 2",
                cost: "dos mil",
                time: "7hrs",
            })
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            })
        });
    });
    describe("Eliminar un servicio", () => {
       it("Debe eliminar un servicio", (done) => {
            chai.request(url)
            .delete('/services/14')
            .set({ 'Authorization': `jwt ${token}` })
            .end((err, res) => {
                //console.log(res.body)
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('mensaje');
                done();
            })
        });
        it("Debe Retornar error al no encontrar el servicio a eliminar", (done) => {
            chai.request(url)
            .delete('/services/26')
            .set({ 'Authorization': `jwt ${token}` })
            .end((err, res) => {
                //console.log(res.body)
                expect(res).to.have.status(404);
                expect(res.body).to.have.property('mensaje');
                done();
            })
        });
        it("Debe Retorna error cuando el token no es valido", (done) => {
            chai.request(url)
            .delete('/services/1')
            .set({ 'Authorization': `jwt ${token}x` })
            .end((err, res) => {
                //console.log(res.body)
                expect(res).to.have.status(401);
                done();
            })
        });
    });
});
