const servicesController = require('../controllers/ServicesController');
const {accessControl} = require('../middlewares/accessControl');

module.exports = (router) => {
    //Rutas de servicios 
    router.post('/services', servicesController.add);
    router.get('/services', servicesController.list);
    router.get('/services/:id', servicesController.show);
    router.put('/services/:id', servicesController.update);
    router.delete('/services/:id', servicesController.delete);
    
    /*router.post('/services', accessControl('createAny', 'services'),  servicesController.add);
    router.get('/services', accessControl('readAny', 'services'), servicesController.list);
    router.get('/services/:id', accessControl('readAny', 'services'), servicesController.show);
    router.put('/services/:id', accessControl('updateAny', 'services'), servicesController.update);
    router.delete('/services/:id', accessControl('deleteAny', 'services'), servicesController.delete);
    */
    return router;
};