const usersController = require('../controllers/UsersController');
const {accessControl} = require('../middlewares/accessControl');

module.exports = (router) => {
    //rutas de users
    router.get('/users/:id', usersController.mostrar);
    router.get('/users', usersController.listar);
    router.put('/users/:id', usersController.actualizar);
    router.delete('/users/:id', usersController.eliminar);

    /*router.get('/users/:id', accessControl('readAny', 'user'), usersController.show);
    router.get('/users', accessControl('readAny', 'user'), usersController.list);
    router.put('/users/:id', accessControl('updateAny', 'user'), usersController.update);
    router.delete('/users/:id', accessControl('deleteteAny', 'user'),usersController.delete);
    */
    return router;
};