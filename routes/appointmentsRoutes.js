const appointmentsController = require('../controllers/AppointmentsController');
const {accessControl} = require('../middlewares/accessControl');

module.exports = (router) => {
    //Rutas de citas
    router.post('/services/:services/appointments', appointmentsController.add);
    router.get('/services/:services/appointments', appointmentsController.list);
    router.get('/services/:services/appointments/:id', appointmentsController.show);
    router.put('/services/:services/appointments/:id', appointmentsController.update);
    router.delete('/services/:services/appointments/:id', appointmentsController.delete);

    /*router.post('/services/:services/appointments', accessControl('createAny', 'appointment'), appointmentsController.add);
    router.get('/services/:services/appointments', accessControl('readAny', 'appointment'), appointmentsController.list);
    router.put('/services/:services/appointments/:id', accessControl('updateAny', 'appointment'), appointmentsController.update);
    router.delete('/services/:services/appointments/:id', accessControl('deleteAny', 'appointment'), appointmentsController.delete);
    */
    return router;
};
