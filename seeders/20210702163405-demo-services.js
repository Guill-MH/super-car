'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
   await queryInterface.bulkInsert('Services', [{
    name: 'Reparacion de Motor',
    cost: '5000',
    time: '24hr',
    createdAt: new Date(),
    updatedAt: new Date()
    },{
    name: 'Cambio de aceite',
    cost: '500',
    time: '4hrs',
    createdAt: new Date(),
    updatedAt: new Date()
    }
  ], {});
    
  },

  down: async (queryInterface, Sequelize) => {
    
    
     

      await queryInterface.bulkDelete('Services', null, {});
     
  }
};
