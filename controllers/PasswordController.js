const bcrypt = require('bcrypt');
const { request } = require('express');
const { Op } = require("sequelize");
const { User } = require('../models')

const { passwordEmail } = require('../utils/passwordEmail');

exports.resetearPassword  = async (request, response, next) => {
try {
  // comprobar que se reciba el email
  if (!request.body.email){
      response.status(400).json({ error: true, message: 'Debe proporcionar el email.' });
    }

      // Buscar el usuario por email
      const user = await User.findOne({
          where: { email: request.body.email}
      });

  if (!user) {
      response.status(404).json({ message: 'No existe el Usario.' });
  }

  // generar el token de recuperacion de contraseña
let token = await  bcrypt.hash(user.email + Date.now().toString(), 10);
//gfdhgfnbfghjfhfg//hfg/h/cdd
token = token.replace(/\//g,"-");
//Save token
user.passwordResetToken = token;
user.passwordResetExperire= Date.now() + 3600000;
await user.save();

//Enviar el Email
const resultadoEmail = await passwordEmail(
    `${user.name}`,
    user.email,
    token
);


if (resultadoEmail) {
    response.json({ message: 'Un mensaje ha sido enviado al email proporcionado' });
} else {
    response.status(503).json({error: true, message: 'Ocurrio un error al enviar al correo de recuperacion.',})
}

} catch (error) {
    console.log(error);
    response.status(503).json({error: true, 
message: 'error al intentar enviar al correo de recuperacion.',})
}

};

exports.validarToken = async (request, response, next) => {
    try {
        // buscar el usuario con el token, y vigencia 
        const user = await User.findOne({
           where: {
               passwordResetToken: request.body.token,
               passwordResetExperire: {[Op.gt]: new Date()},
           }
        });
    
    if (!user) {
        response.status(400).json ({
            message: 'El Link de restablecer contraseña es invalido'
        });
    }
    
    // retornar un status que indique si es valido
    response.json({
        isValid: true,
        message: 'Ingrese una nueva contraseña.',
    });
    } catch {
    response.status(503).json({ message: 'error al validar el token' });
    }
    };
    

exports.guardarNuevoPassword = async (request, response, next) => {
try {
// volver a validar el token
const user = await User.findOne({
    where: {
        passwordResetToken: request.body.token,
        passwordResetExperire: {[Op.gt]: new Date()},
    }
 });

if (!user) {
 response.status(400).json ({message: 'El Link de restablecer contraseña es invalido'});
}

// validar que se reciba la contraseña
if (!request.body.password) {
    response.status(400).json({ message: 'La contraseña es obligatoria.' });
}

// cifrar la contraseña
const salt = await bcrypt.genSalt(10);

// cifrar la contraseña 
user.password = await bcrypt.hash(request.body.password, salt);

// quitar el token de recuperacion
user.passwordResetToken = '';
user.passwordResetExpire = null;

// guardar cambios 
await user.save();

response.json({ message: 'La nueva contraseña ha sido guardada, puede Iniciar sesion' });

} catch (error) {
    console.log(error);
    response.status(503).json({ message: 'error al actualizar contraseña' });
  }
};
