const {Services} = require('../models');
const {sendPushNotification}= require('../utils/notificationsPush');
//Agregar  servicio
exports.add = async(req, res, next) => {
    try {
        //crear un servicio
       const service = await Services.create(req.body);
       
       const subscription = { endpoint:
        'https://fcm.googleapis.com/fcm/send/c-cmnDAb1OY:APA91bHxQmR76iH_HiXf_1drfIoeO41WKlQ9EZNZJwC2DUEQSV-p8PW61Aa8hfr8TV-3BWZCPRkyqUtqcK5-M1AnSNlb5YBQ8aGPaQ8vQp2azImtzDL4xA_adxRUDJ9VbcihLZYmCCnE',
       expirationTime: null,
       keys:
        { p256dh:
           'BOQtYn0fWdfAFxYty9YfS9-iITP1SwMgYWeVIrdzmMpM6BhDQF2JyoDdQAcFdlhKJPOjmb10BpulM6q-KebjAvg',
          auth: 'Ej5k4Twcxq-CqTWNj6_YDg' }
         };
          //recuperar subscripcion
        sendPushNotification(
        subscription,
        "Perfil Creado", `${req.body.name} bienvenido`,
        );
    
        res.json({ message: 'Se agregó el servicio.',  service, subscription, })
       
    } catch (error) { 
           console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        
        res.json({ error: true,message: 'Error al agregar Servicio',errores,});
        next();
    }
};

//listar servicio
exports.list = async(req, res, next) => {
    try {
        //extraer la lista de servicios
        const services = await Services.findAll({});
        res.json(services);
    } catch (error) {
        console.error(error);
        res.json({ mensaje: 'Error al leer  servicio'});
        next();
    }
};

//mostrar / leer / :id
exports.show = async(req, res, next) => {
    try {
        //Buscar el registro, por id
        const services = await Services.findByPk(req.params.id);
        if (!services) {
            res.status(404).json({error: true, mensaje: 'No se encontro el servicio. '});
        } else {
            res.json(services);
        }
    } catch (error) {
        res.status(503).json({ error : true, mensaje: 'Error al leer servicio'});
        console.error(error);
    }
};
//Actualizar servicio
exports.update = async(req, res, next) => {
    try {
        //Obtener el registro de los servicios desde la bd
        const services = await Services.findByPk(req.params.id);
        if (!services) {
            res.status(404).json({error: true, mensaje: 'No se encontro  el servicio. '});
        } else {
            

            Object.keys(req.body).forEach((propiedad) => {
                services[propiedad] = req.body[propiedad];
            })

            //guardar los cambios
            await services.save();
            res.json({ mensaje: 'El registro fue actualizado', services});
        }
    } catch (error) {
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.status(403).json({ 
            error: true,
            message: 'Error al actualizar la servicio',
            errores,
        });
    }
};
//Eliminar servicio
exports.delete = async(req, res, next) => {
    try {
        //Eliminar registro, por id
        const services = await Services.findByPk(req.params.id);
        if (!services) {
            res.status(404).json({ mensaje: 'No se encontro el servicio. '});
        } else {
            await services.destroy();
            res.json({ mensaje: 'Se elimino  el servicio. '});
        }
    } catch (error) {
        res.status(503).json({ error: true,mensaje: 'Error al eliminar el servicio.'});
    }
};
