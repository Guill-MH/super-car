'use strict';

module.exports = (sequelize, DataTypes) => {
  const Appointment = sequelize.define ('Appointment', {
    ServicesId: DataTypes.INTEGER,
    modelCar: DataTypes.STRING,
    telephone: DataTypes.INTEGER,
    commentary: DataTypes.STRING
  }, {
    defaultScope: {
      attributes: ['id', 'ServicesId', 'modelCar', 'telephone', 'commentary'],
    }
  });

  Appointment.associate = function(models) {
    models.Appointment.belongsTo(models.Services);
  };

  return Appointment;
};